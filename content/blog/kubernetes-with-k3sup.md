---
categories:
- Servers
- Kubernetes
date: "2022-01-24"
description: How to use k3sup to deploy a k3s Kubernetes cluster.
summary:  How to use k3sup to deploy a k3s Kubernetes cluster.
slug: kubernetes-with-k3sup
tags:
- docker
- k3s
- kubernetes
- cluster
title: Kubernetes cluster with k3sup
---
# k3sup Cluster Setup
This is a quick howto guid for setting up a lite kubernetes cluster using a helper script called k3sup and ranchers k3s. 

[k3sup](https://github.com/alexellis/k3sup/blob/master/docs/k3sup-rpi.png)

## Install k3sup
```bash
curl -sLS https://get.k3sup.dev | sh
sudo install k3sup /usr/local/bin/

k3sup --help
```
## Enable Passwordless Sudo
- Generate a ssh key locally and then add it to the remote host.
- Install curl on the remote host. 


For `k3sup` to remotely setup nodes and servers it requires your user to have passwordless sudo permissions.
```bash
sudo su
# Enter your password
vim /etc/sudoers
sudo vim /etc/sudoers
```

Modify the `/etc/sudoers` file needs to include `MY_USER ALL=(ALL) NOPASSWD: ALL` towards the end. Replace `MY_USER` with the user you will be using for the remote setup.

```
# replace "MY_USER" with your username i.e. "ubuntu"
MY_USER ALL=(ALL) NOPASSWD: ALL
```

## Install the Master Server

If you are installing it locally on the machine that has k3sup run the following.
``` bash
k3sup install --local
```
If you are setting up a remote host as a server run the following commend replacing `REMOTE_SSH_HOST` with the host IP or name you are targeting. 

```bash
k3sup install --user MY_USERNAME --host REMOTE_SSH_HOST
```

## Add a k3s Node

```bash  
export SERVER_IP=10.0.1.3
export AGENT_IP=10.0.1.3
export USER=dchadwick
k3sup join --ip $AGENT_IP --server-ip $SERVER_IP --user $USER
```  

## Verify the cluster is running
```bash
sudo k3s kubectl cluster-info
```

```bash
kubectl get namespace "portainer" -o json \
  | tr -d "\n" | sed "s/\"finalizers\": \[[^]]\+\]/\"finalizers\": []/" \
  | kubectl replace --raw /api/v1/namespaces/portainer/finalize -f -
```
